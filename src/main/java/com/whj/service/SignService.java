package com.whj.service;

import org.springframework.stereotype.Service;

/**
 * @author 王恒杰
 * @date 2022/10/24 13:59
 * @Description:
 */
@Service
public interface SignService {
    /**
     * 上传签字图片
     *
     * @param img
     * @return
     */
    public String uploadSign(String img);

    /**
     * 盖章
     *
     * @param id
     * @return
     */
    public String sign();
}
