package com.whj;

import com.whj.util.WaterMark;

/**
 * @author 王恒杰
 * @date 2022/10/24 15:18
 * @Description: 测试pdf文件加水印
 */
public class TestWaterMark {
    public static void main(String[] args) {
        /**
         * pdf生成水印
         *
         * @param srcPdfPath       插入前的文件路径
         * @param tarPdfPath       插入后的文件路径
         * @param WaterMarkContent 水印文案
         * @param numberOfPage     每页需要插入的条数
         * @throws Exception
         */
        String srcPdfPath = "D:\\Idea\\stamp\\Itext\\src\\main\\resources\\pdf\\signOut.pdf";
        String tarPdfPath = "D:\\Idea\\stamp\\Itext\\src\\main\\resources\\pdf\\WaterMark.pdf";
        String WaterMarkContent = "程序员小王";
        Integer numberOfPage = 3;
        try {
            WaterMark.addWaterMark(srcPdfPath, tarPdfPath, WaterMarkContent, numberOfPage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
